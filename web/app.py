import os
import datetime
import hashlib
import cchardet

from flask import Flask, render_template, request, redirect, url_for, jsonify, json
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager, current_user, login_user, logout_user, login_required
from werkzeug import secure_filename

from config import SECRET_KEY, SQLALCHEMY_DATABASE_URI, CSRF_ENABLED, CSRF_SESSION_KEY, UPLOAD_FOLDER, \
    ALLOWED_EXTENSIONS, BASE_DIR, RESULT_PER_PAGE, DEBUG

app = Flask(__name__)
app.secret_key = SECRET_KEY
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['CSRF_ENABLED'] = CSRF_ENABLED
app.config['CSRF_SESSION_KEY'] = CSRF_SESSION_KEY

# Debug
if DEBUG:
    app.debug = True
    app.config['SQLALCHEMY_ECHO'] = True

db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.session_protection = "strong"
login_manager.login_view = "/"

# DB Model
from modules.user.model import User
from modules.brands.model import Brands, BrandCategory
from modules.products.model import Products, ProductImages


@app.route("/", methods=["GET", "POST"])
def index():
    # Test to register 1 user
    # admin = User("admin", create_signature("root"), "zaki.haris@gmail.com", 1000, "", "", "")
    # db.session.add(admin)
    # db.session.commit()

    if current_user.is_authenticated:
        return redirect(url_for("home"))

    if request.method == 'POST':
        next_url = request.args.get("next", "/home")
        username = request.form.get("username", None)
        passwd = request.form.get("password", None)
        sign = create_signature(convert_encoding(passwd))

        if username is not None:
            user = User.query.filter_by(username=username, password=sign).first()
            if user is not None:
                if login_user(user):
                    return redirect(next_url)
            else:
                print "User not found"

    return render_template("login.html", **{})


@app.route("/home", methods=["GET"])
@login_required
def home():
    brands = Brands.query.all()

    return render_template("home.html", **{
        "current_user": current_user,
        "inPage": "home",
        "brands": brands,
    })


@app.route("/brand/<brand_id>", methods=["GET"])
@login_required
def brands(brand_id):
    brand = Brands.query.filter_by(id=brand_id).first()
    brands_categories = BrandCategory.query.join(Brands).filter_by(id=brand_id).all()

    return render_template("brands.html", **{
        "current_user": current_user,
        "inPage": "home",
        "brand": brand,
        "brands_categories": brands_categories,
    })


@app.route("/brand/<brand_id>/category/<category_id>/page/<int:page>", methods=["GET"])
@login_required
def brands_categories(brand_id, category_id, page):
    offset = RESULT_PER_PAGE * (page-1)
    brand = Brands.query.filter_by(id=brand_id).first()
    category = BrandCategory.query.filter_by(id=category_id).first()

    total_products = db.session.execute(
        """
        SELECT COUNT(p.id) as total
        FROM products p
        INNER JOIN brands b on p.brand_id = b.id
        INNER JOIN brand_category c on p.category_id = c.id
        WHERE p.brand_id = :brand_id
        AND p.category_id = :category_id
        """, {"brand_id": brand_id, "category_id": category_id}
    )

    for i in total_products:
        total_page = i.total / RESULT_PER_PAGE
        reminder = i.total % RESULT_PER_PAGE
        if total_page > 0 and reminder > 0:
            total_page += 1

    products = db.session.execute(
        """
        SELECT p.id, p.brand_id, p.category_id, p.part_number, p.desc, p.price, p.subcategory, p.tags,
               b.name as brand_name, b.logo as brand_logo, c.name as category_name
        FROM products p
        INNER JOIN brands b on p.brand_id = b.id
        INNER JOIN brand_category c on p.category_id = c.id
        WHERE p.brand_id = :brand_id
        AND p.category_id = :category_id
        ORDER BY p.subcategory
        LIMIT :result_per_page OFFSET :offset
        """, {"brand_id": brand_id, "category_id": category_id, "result_per_page": RESULT_PER_PAGE, "offset": offset}
    )

    products_list = []
    for p in products:
        d = dict(p.items())
        pictures = ProductImages.query.filter_by(product_id=p.id).all()
        d["pictures"] = pictures
        products_list.append(d)

    page_url = "/brand/%s/category/%s/page/" % (brand_id, category_id)

    return render_template("categories.html", **{
        "current_user": current_user,
        "inPage": "home",
        "brand": brand,
        "category": category,
        "products": products_list,
        "page_url": page_url,
        "page": page,
        "total_page": total_page
    })


@app.route("/products/<keyword>/page/<int:page>", methods=["GET"])
@login_required
def products(keyword, page):
    offset = RESULT_PER_PAGE * (page-1)

    total_products = db.session.execute(
        """
        SELECT COUNT(p.id) as total
        FROM products p
        INNER JOIN brands b on p.brand_id = b.id
        INNER JOIN brand_category c on p.category_id = c.id
        WHERE p.part_number LIKE :keyword
        OR p.desc LIKE :keyword
        OR p.subcategory LIKE :keyword
        OR p.tags LIKE :keyword
        OR b.name LIKE :keyword
        OR c.name LIKE :keyword
        ORDER BY b.name, c.name, p.subcategory
        """, {"keyword": '%' + keyword + '%'}
    )

    for i in total_products:
        total_result = i.total
        total_page = i.total / RESULT_PER_PAGE
        reminder = i.total % RESULT_PER_PAGE
        if total_page > 0 and reminder > 0:
            total_page += 1

    products = db.session.execute(
        """
        SELECT p.id, p.brand_id, p.category_id, p.part_number, p.desc, p.price, p.subcategory, p.tags,
               b.name as brand_name, b.logo as brand_logo, c.name as category_name
        FROM products p
        INNER JOIN brands b on p.brand_id = b.id
        INNER JOIN brand_category c on p.category_id = c.id
        WHERE p.part_number LIKE :keyword
        OR p.desc LIKE :keyword
        OR p.subcategory LIKE :keyword
        OR p.tags LIKE :keyword
        OR b.name LIKE :keyword
        OR c.name LIKE :keyword
        ORDER BY b.name, c.name, p.subcategory
        LIMIT :result_per_page OFFSET :offset
        """, {"result_per_page": RESULT_PER_PAGE, "offset": offset, "keyword": '%' + keyword + '%'}
    )

    products_list = []
    for p in products:
        d = dict(p.items())
        pictures = ProductImages.query.filter_by(product_id=p.id).all()
        d["pictures"] = pictures
        products_list.append(d)

    page_url = "/products/%s/page/" % keyword

    return render_template("products.html", **{
        "current_user": current_user,
        "inPage": "home",
        "products": products_list,
        "keyword": keyword,
        "page_url": page_url,
        "page": page,
        "total_page": total_page,
        "total_result": total_result
    })


@app.route("/me/edit", methods=["GET", "POST"])
@login_required
def me_edit():
    success = False

    if request.method == "POST":
        email = request.form.get("email", None)
        first_name = request.form.get("first_name", None)
        last_name = request.form.get("last_name", None)
        about = request.form.get("about", None)
        address = request.form.get("address", None)
        contact = request.form.get("contact", None)
        passwd1 = request.form.get("password1", None)
        passwd2 = request.form.get("password2", None)
        passwd3 = request.form.get("password3", None)

        current_user.email = email
        current_user.first_name = first_name
        current_user.last_name = last_name
        current_user.about = about
        current_user.address = address
        current_user.contact = contact
        db.session.commit()

        if passwd1 is not None and current_user.password == create_signature(convert_encoding(passwd1)):
            if passwd2 == passwd3:
                current_user.password = create_signature(convert_encoding(passwd3))
                db.session.commit()

        success = True

    return render_template("me/user_edit.html", **{
        "current_user": current_user,
        "inPage": "user",
        "success": success
    })


@app.route("/admin/user", methods=["GET"])
@login_required
def admin_user():
    users = User.query.all()

    return render_template("user/user.html", **{
        "current_user": current_user,
        "inPage": "user",
        "users": users
    })


@app.route("/admin/user/<action_uri>", methods=["GET", "POST"])
@login_required
def admin_user_edit(action_uri):
    user = None

    if request.method == "GET":
        if action_uri == "add":
            return render_template("user/user_add.html", **{
                "current_user": current_user,
                "inPage": "user",
            })

        user = User.query.filter_by(id=action_uri).first()

    if request.method == "POST":
        if action_uri == "add":
            error = []
            username = request.form.get("username", None)
            email = request.form.get("email", None)
            first_name = request.form.get("first_name", None)
            last_name = request.form.get("last_name", None)
            about = request.form.get("about", None)
            address = request.form.get("address", None)
            contact = request.form.get("contact", None)
            passwd1 = request.form.get("password1", None)
            passwd2 = request.form.get("password2", None)

            check_username = User.query.filter_by(username=username).first()
            if check_username is not None:
                error.append("Username already exists.")

            check_email = User.query.filter_by(email=email).first()
            if check_email is not None:
                error.append("Email already exists.")

            if passwd1 != passwd2:
                error.append("Your password and confirmation password do not match.")

            if error:
                return render_template("user/user_add.html", **{
                    "current_user": current_user,
                    "inPage": "user",
                    "error": error
                })

            new_user = User(username, create_signature(convert_encoding(passwd2)), email, 1, first_name, last_name,
                            about, address, contact)
            db.session.add(new_user)
            db.session.commit()
        else:
            user = User.query.filter_by(id=action_uri).first()
            email = request.form.get("email", None)
            first_name = request.form.get("first_name", None)
            last_name = request.form.get("last_name", None)
            about = request.form.get("about", None)
            address = request.form.get("address", None)
            contact = request.form.get("contact", None)
            passwd1 = request.form.get("password1", None)
            passwd2 = request.form.get("password2", None)
            passwd3 = request.form.get("password3", None)

            user.email = email
            user.first_name = first_name
            user.last_name = last_name
            user.about = about
            user.address = address
            user.contact = contact
            db.session.commit()

            if passwd1 is not None and user.password == create_signature(convert_encoding(passwd1)):
                if passwd2 == passwd3:
                    user.password = create_signature(convert_encoding(passwd3))
                    db.session.commit()

        return redirect(url_for("admin_user"))

    return render_template("user/user_edit.html", **{
        "current_user": current_user,
        "inPage": "user",
        "action_uri": action_uri,
        "user": user,
    })


@app.route("/admin/user/delete/<user_id>", methods=["GET"])
@login_required
def admin_user_delete(user_id):
    User.query.filter_by(id=user_id).delete()
    db.session.commit()

    return redirect(url_for("admin_user"))


@app.route("/admin/brands", methods=["GET"])
@login_required
def admin_brands():
    brands = Brands.query.all()

    return render_template("brands/brands.html", **{
        "current_user": current_user,
        "inPage": "brands",
        "brands": brands
    })


@app.route("/admin/brands/<action_uri>", methods=["GET", "POST"])
@login_required
def admin_brands_edit(action_uri):
    brand = None

    if request.method == "GET":
        if action_uri == "add":
            return render_template("brands/brands_add.html", **{
                "current_user": current_user,
                "inPage": "brand",
            })

        brand = Brands.query.filter_by(id=action_uri).first()

    if request.method == "POST":
        if action_uri == "add":
            error = []
            name = request.form.get("name", None)
            logo = request.files["logo"]

            check_name = Brands.query.filter_by(name=name).first()
            if check_name is not None:
                error.append("Brand already exists.")

            if logo and not allowed_file(logo.filename):
                error.append("Only .png, .jpg, .jpeg, .gif is allowed.")

            if error:
                return render_template("brands/brands_add.html", **{
                    "current_user": current_user,
                    "inPage": "brands",
                    "error": error
                })

            if logo:
                save_uploaded_image(logo)
                now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M')
                logo_name = secure_filename(now + "_" + logo.filename)
            else:
                logo_name = None

            new_brand = Brands(name, logo_name)
            db.session.add(new_brand)
            db.session.commit()
        else:
            brand = Brands.query.filter_by(id=action_uri).first()
            brand.name = request.form.get("name", None)
            db.session.commit()

            logo = request.files["logo"]
            if logo and allowed_file(logo.filename):
                save_uploaded_image(logo)
                now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M')
                brand.logo = secure_filename(now + "_" + logo.filename)
                db.session.commit()

        return redirect(url_for("admin_brands"))

    return render_template("brands/brands_edit.html", **{
        "current_user": current_user,
        "inPage": "brands",
        "action_uri": action_uri,
        "brand": brand
    })


@app.route("/admin/brands/delete/<brand_id>", methods=["GET"])
@login_required
def admin_brands_delete(brand_id):
    Brands.query.filter_by(id=brand_id).delete()
    db.session.commit()

    return redirect(url_for("admin_brands"))


@app.route("/admin/brands/categories", methods=["GET"])
@login_required
def admin_brands_categories():
    brands_categories = BrandCategory.query.join(Brands).all()

    return render_template("brands/brands_categories.html", **{
        "current_user": current_user,
        "inPage": "brands_categories",
        "brands_categories": brands_categories,
    })


@app.route("/admin/brands/categories/<action_uri>", methods=["GET", "POST"])
@login_required
def admin_brands_categories_edit(action_uri):
    brands = Brands.query.all()
    brands_categories = None

    if request.method == "GET":
        if action_uri == "add":
            return render_template("brands/brands_categories_add.html", **{
                "current_user": current_user,
                "inPage": "brands_categories",
                "brands": brands,
            })

        brands_categories = BrandCategory.query.join(Brands).filter(BrandCategory.id == action_uri).first()

    if request.method == "POST":
        if action_uri == "add":
            name = request.form.get("name", None)
            brand_id = request.form.get("brand_id", None)

            new_category = BrandCategory(name, brand_id)
            db.session.add(new_category)
            db.session.commit()
        else:
            brands_categories = BrandCategory.query.filter_by(id=action_uri).first()
            brands_categories.name = request.form.get("name", None)
            brands_categories.brand_id = request.form.get("brand_id", None)
            db.session.commit()

        return redirect(url_for("admin_brands_categories"))

    return render_template("brands/brands_categories_edit.html", **{
        "current_user": current_user,
        "inPage": "brands_categories",
        "action_uri": action_uri,
        "brands_categories": brands_categories,
        "brands": brands,
    })


@app.route("/admin/brands/categories/delete/<category_id>", methods=["GET"])
@login_required
def admin_brands_categories_delete(category_id):
    BrandCategory.query.filter_by(id=category_id).delete()
    db.session.commit()

    return redirect(url_for("admin_brands_categories"))


@app.route("/admin/products/add", methods=["GET", "POST"])
@login_required
def admin_products_add():
    brands = Brands.query.all()
    subcat = db.session.execute("SELECT DISTINCT subcategory from products ORDER BY subcategory")

    if request.method == "POST":
        error = []
        brand_id = request.form.get("brand_id", None)
        category_id = request.form.get("category_id", None)
        part_number = request.form.get("part_number", None)
        desc = request.form.get("desc", None)
        price = request.form.get("price", None)
        subcategory = request.form.get("subcategory", None)
        tags = request.form.get("tags", None)
        pictures = request.files.getlist("pictures")

        if not part_number:
            error.append("Please enter part number.")

        if not desc:
            error.append("Please enter description.")

        if not price:
            error.append("Please enter price.")

        for picture in pictures:
            if picture and not allowed_file(picture.filename):
                error.append("Only .png, .jpg, .jpeg, .gif is allowed.")
                break

        if error:
            return render_template("products/products_add.html", **{
                "current_user": current_user,
                "inPage": "products",
                "brands": brands,
                "subcat": subcat,
                "error": error
            })

        product = Products(brand_id, category_id, part_number, desc, price, subcategory, tags)
        db.session.add(product)
        db.session.commit()

        for pic in pictures:
            if pic:
                save_uploaded_image(pic)
                now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M')
                pic_name = secure_filename(now + "_" + pic.filename)

                # Save image to DB
                new_pic = ProductImages(product.id, pic_name)
                db.session.add(new_pic)
                db.session.commit()

    return render_template("products/products_add.html", **{
        "current_user": current_user,
        "inPage": "products",
        "brands": brands,
        "subcat": subcat,
    })


@app.route("/admin/products/edit/<product_id>", methods=["GET", "POST"])
@login_required
def admin_products_edit(product_id):
    prev_url = "/"
    brands = Brands.query.all()
    subcat = db.session.execute("SELECT DISTINCT subcategory from products ORDER BY subcategory")
    product = Products.query.filter_by(id=product_id).first()
    brands_categories = BrandCategory.query.join(Brands).filter_by(id=product.brand_id).all()
    pictures = ProductImages.query.filter_by(product_id=product.id).all()

    if request.method == "GET":
        prev_url = request.referrer

    if request.method == "POST":
        error = []
        prev_url = request.form.get("next")
        brand_id = request.form.get("brand_id", None)
        category_id = request.form.get("category_id", None)
        part_number = request.form.get("part_number", None)
        desc = request.form.get("desc", None)
        price = request.form.get("price", None)
        subcategory = request.form.get("subcategory", None)
        tags = request.form.get("tags", None)
        _pictures = request.files.getlist("pictures")

        if not part_number:
            error.append("Please enter part number.")

        if not desc:
            error.append("Please enter description.")

        if not price:
            error.append("Please enter price.")

        for picture in _pictures:
            if picture and not allowed_file(picture.filename):
                error.append("Only .png, .jpg, .jpeg, .gif is allowed.")
                break

        if error:
            return render_template("products/products_add.html", **{
                "current_user": current_user,
                "inPage": "products",
                "brands": brands,
                "brands_categories": brands_categories,
                "subcat": subcat,
                "product": product,
                "pictures": pictures,
                "error": error,
                "prev_url": prev_url,
            })

        product.brand_id = brand_id
        product.category_id = category_id
        product.part_number = part_number
        product.desc = desc
        product.price = price
        product.subcategory = subcategory
        product.tags = tags
        db.session.commit()

        for pic in _pictures:
            if pic:
                save_uploaded_image(pic)
                now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M')
                pic_name = secure_filename(now + "_" + pic.filename)

                # Save image to DB
                new_pic = ProductImages(product.id, pic_name)
                db.session.add(new_pic)
                db.session.commit()

        return redirect(prev_url)

    return render_template("products/products_edit.html", **{
        "current_user": current_user,
        "inPage": "products",
        "brands": brands,
        "brands_categories": brands_categories,
        "subcat": subcat,
        "product": product,
        "pictures": pictures,
        "prev_url": prev_url,
    })


@app.route("/admin/products/delete/<product_id>", methods=["GET"])
@login_required
def admin_products_delete(product_id):
    Products.query.filter_by(id=product_id).delete()
    ProductImages.query.filter_by(product_id=product_id).delete()
    db.session.commit()

    return redirect(request.referrer)


@app.route("/admin/products/images/delete/<image_id>", methods=["GET"])
@login_required
def admin_products_images_delete(image_id):
    ProductImages.query.filter_by(id=image_id).delete()
    db.session.commit()

    return redirect(request.referrer)


# API
@app.route("/api/get/category/<brand_id>", methods=["GET"])
def api_get_category(brand_id):
    categories = BrandCategory.query.filter_by(brand_id=brand_id).all()
    html = '<option value="0">Select Category</option>'
    for c in categories:
        html += '<option value="%s">%s</option>' % (c.id, c.name)

    return html


@login_manager.user_loader
def load_user(id):
    return User.query.filter_by(id=id).first()


def logout():
    logout_user()
    return redirect("/")


def create_signature(data):
    return hashlib.sha1(repr(data) + "," + SECRET_KEY).hexdigest()


def verify_signature(data, signature):
    return signature == create_signature(data)


def convert_encoding(data, new_coding='UTF-8'):
    encoding = cchardet.detect(data)['encoding']

    if new_coding.upper() != encoding.upper():
        data = data.decode(encoding, data).encode(new_coding)

    return data


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def save_uploaded_image(img):
    img_name = secure_filename(datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M') + "_" + img.filename)
    img_path = os.path.join(BASE_DIR, UPLOAD_FOLDER, img_name)
    img.save(img_path)


app.add_url_rule('/logout', "logout_user", logout, methods=['GET'])
