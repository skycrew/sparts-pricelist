/**
 * Created by zaki on 16/01/2016.
 */

$(document).ready(function() {
    $("#brand_id").change(function (){
        var brand_id = this.value;

        $.ajax({
            type: "get",
            url: "/api/get/category/" + brand_id,
            dataType: "html",
            success: function(data) {
                console.log(data);
                $("#category_id").html(data);
            },
            error: function() {
                alert("Unable to get categories");
            }
        });
    });

    $("#search").keyup(function(event){
         if(event.keyCode == 13){
             window.location = "/products/" + this.value + "/page/1";
         }
    });
});