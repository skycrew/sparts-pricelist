import datetime

from flask.ext.login import UserMixin
from web.app import db


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(255))
    email = db.Column(db.String(120), index=True, unique=True)
    first_name = db.Column(db.String(255), nullable=True)
    last_name = db.Column(db.String(255), nullable=True)
    role = db.Column(db.Integer, default=1)
    about = db.Column(db.String(2000), nullable=True)
    address = db.Column(db.String(255), nullable=True)
    contact = db.Column(db.String(20), nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __init__(self, username, password, email, role, first_name, last_name, about, address, contact):
        self.username = username
        self.password = password
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.role = role
        self.about = about
        self.address = address
        self.contact = contact

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.username)
