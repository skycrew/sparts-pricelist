from web.app import db
# userList = users.query.join(friendships, users.id==friendships.user_id).add_columns(users.userId, users.name,
# users.email, friends.userId, friendId).filter(users.id == friendships.friend_id)
# .filter(friendships.user_id == userID).paginate(page, 1, False)


class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    brand_id = db.Column(db.Integer)
    category_id = db.Column(db.Integer)
    part_number = db.Column(db.String(100), index=True)
    desc = db.Column(db.String(1000), index=True)
    price = db.Column(db.Float)
    subcategory = db.Column(db.String(255), nullable=True)
    tags = db.Column(db.String(255), index=True, nullable=True)

    def __init__(self, brand_id, category_id, part_number, desc, price, subcategory=None, tags=None):
        self.brand_id = brand_id
        self.category_id = category_id
        self.part_number = part_number
        self.desc = desc
        self.price = price
        self.subcategory = subcategory
        self.tags = tags

    def __repr__(self):
        return '<Product %r>' % (self.part_number)


class ProductImages(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    product_id = db.Column(db.Integer, index=True)
    path = db.Column(db.String(255))

    def __init__(self, product_id, path):
        self.product_id = product_id
        self.path = path

    def __repr__(self):
        return '<Product Image %r>' % (self.path)
