from web.app import db


class Brands(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), index=True)
    logo = db.Column(db.String(255), default="/static/img/default.jpg")
    categories = db.relationship("BrandCategory", backref=db.backref("brands", lazy="joined"), lazy="dynamic")

    def __init__(self, name, logo):
        self.name = name
        self.logo = logo

    def __repr__(self):
        return '<Brands %r>' % (self.name)


class BrandCategory(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), index=True)
    brand_id = db.Column(db.Integer, db.ForeignKey("brands.id"))

    def __init__(self, name, brand_id):
        self.name = name
        self.brand_id = brand_id

    def __repr__(self):
        return '<Brand Category %r>' % (self.name)

    @property
    def serialize(self):
        return {
            "id": self.id,
            "name": self.name
        }
