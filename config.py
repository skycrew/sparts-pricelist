import os

# Statement for enabling the development environment
DEBUG = True

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "eq6@()h$apcbxq(+vs22wps+0jyl$)gwd@8fi=m9@74lzwpnbl"

# Secret key for signing cookies
SECRET_KEY = "&hqn61@@_6!ely74eez(b_42ch(b*x0$jtwe0=6wxto!np1g4k"

# DB
SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:@localhost/sparts_pricelist"

# Upload Config
UPLOAD_FOLDER = "web/static/img"
ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg", "gif"])

# Result per page
RESULT_PER_PAGE = 2
