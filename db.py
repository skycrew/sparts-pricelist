from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from web.app import app, db
from web.modules.user import model
from web.modules.brands import model
from web.modules.products import model


migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
